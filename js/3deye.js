(function ($) {
	$.fn.extend({
		vc3dEye: function(params) {
			vc3dEye(this.selector, params);
			return
		}
	});
	item="Black/";
	var vc3dEye=function (selectorName,params) {	
		//assigning params
		this.selector = $(selectorName);
		this.imagePath = params.imagePath;
		this.totalImages = params.totalImages;
		this.imageExtension = params.imageExtension || "png";
		this.isMoving = false;
		this.currentX = 0;
		this.currentImage=1;
		var direction;
		var scroll;
		var lastScroll;
		$(function() {
				lastScroll = 0;
				var down = false;
				$(window).on("scroll", function() {
					scroll = $(this).scrollTop();
					down = scroll > lastScroll;
					lastScroll = scroll;
					loadAppropriateImage2(down);
			})
			})
		/*function assignOperations() {
			selector.mousedown(function(target) {
				isMoving = true;
				currentX=target.pageX - this.offsetLeft;
				//  console.log("mousedown : isMoving="+isMoving);
			});

			$(document).mouseup(function() {
				isMoving = false;
				//console.log("mouseup : isMoving="+isMoving);
			});

			selector.mousemove(function(target) {
				//console.log("mousemove : isMoving="+isMoving);
				if (isMoving == true) 
					loadAppropriateImage(target.pageX - this.offsetLeft);
				// else 
				// 	currentX = target.pageX - this.offsetLeft
			});

			selector.bind("touchstart", function(target) {
				console.log("touchstart : isMoving="+isMoving);
				isMoving = true;

				//store the start position
				var actualTouch = target.originalEvent.touches[0] || target.originalEvent.changedTouches[0];
				currentX = actualTouch.clientX;


			});
			
			$(document).bind("touchend", function() {
				console.log("touchend : isMoving="+isMoving);
				isMoving = false;
			});

			selector.bind("touchmove", function(target) {
				console.log("touchmove : isMoving="+isMoving);
				target.preventDefault();
				var actualTouch = target.originalEvent.touches[0] || target.originalEvent.changedTouches[0];
				if (isMoving == true) 
					loadAppropriateImage(actualTouch.pageX - this.offsetLeft);
				else 
					currentX = actualTouch.pageX - this.offsetLeft
			})
		}*/
		function loadAppropriateImage2(moving){
			console.log(scroll);
			if(scroll< 4500 ){
				if(moving==true){
					if(lastScroll - scroll < 33){	
						lastScroll=scroll;
						currentImage=--currentImage< 1 ? totalImages : currentImage;
						console.log("currentImage="+currentImage)
						selector.css("background-image", "url(" + imagePath +item+ currentImage + "." + imageExtension + ")");
						$(".center").css("position","fixed");
						$(".center").css("top",'50%');
					}
				}
					 else if( lastScroll-scroll>-33){
						lastScroll=scroll;
						currentImage=++currentImage > totalImages ? 1 : currentImage;
						console.log("currentImage="+currentImage)
						selector.css("background-image", "url(" + imagePath+ item+ currentImage + "." + imageExtension + ")");
						$(".center").css("position","fixed");
						$(".center").css("top",'50%');
						}
			}
			else {
				console.log("mas de 4500");
				$(".center").css("position","absolute");
				$(".center").css("top",4800);
				//selector.css("background-image", "url(" + imagePath+ item+ 0 + "." + imageExtension + ")");
				//selector.css("position","relative");
				}
		}
		/*
		function loadAppropriateImage(newX) {

			if (currentX - newX > 25 ) {
				console.log("currentX =" + currentX +" newX =" +newX)
				console.log("currentX-newX="+ (currentX - newX) );
				currentX = newX;
				currentImage = --currentImage < 1 ? totalImages : currentImage;
				console.log("currentImage="+currentImage)
				selector.css("background-image", "url(" + imagePath + currentImage + "." + imageExtension + ")");
			} else if (currentX - newX < -25) {
				console.log("currentX =" + currentX +" newX =" +newX)
				console.log("currentX-newX="+ (currentX - newX) );
				currentX = newX;
				currentImage = ++currentImage > totalImages ? 1 : currentImage;  
				console.log("currentImage="+currentImage)              
				selector.css("background-image", "url(" + imagePath + currentImage + "." + imageExtension + ")");
			}
		}*/
		function forceLoadAllImages() {
			//load the first image
			 
			var loadedImages = 2;
			var appropriateImageUrl = imagePath+item + "1." + imageExtension;
			selector.css("background-image", "url(" + appropriateImageUrl + ")");
			selector.css("background-size",500,500);
			selector.css("background-position","center");
			selector.css("background-repeat","no-repeat");

			$("<img/>").attr("src", appropriateImageUrl).load(function() {
				//selector.height(500).width(500);
			});
			
			//load all other images by force
			for (var n = 2; n <= totalImages; n++) {
				appropriateImageUrl = imagePath +item+ n+"." + imageExtension;
				selector.append("<img src='" + appropriateImageUrl + "' style='display:none;'>");
				$("<img/>").attr("src", appropriateImageUrl).css("display", "none").load(function() {
					loadedImages++;
					if (loadedImages >= totalImages) {
						$(".center").removeClass("onLoadingDiv");
						$(".center").text("")
					}
				})
			}
		}

		function initializeOverlayDiv() {
			$("html").append("<style type='text/css'>.onLoadingDiv{background-color:#00FF00;opacity:0.5;text-align:center;font-size:2em;font:color:#000000;}</style>")
			$(selector).html("<div id='VCc' style='height:0%;width:0%;' class='onLoadingDiv'></div>");
		}

		initializeOverlayDiv();
		forceLoadAllImages();
		//loadAppropriateImage();
		//assignOperations();
	}

 
})(jQuery);

